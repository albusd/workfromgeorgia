<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'traveler' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~yTk!^[>7!gsfhb)%a>D>C,6pRTY=sJ5v4{b|&4;*Bt}. wLB%4QR}5jBpaK!q)+' );
define( 'SECURE_AUTH_KEY',  'uv}}eJK1aI{&Yx90W&1Ag);LK$6tVv8sw$3:x_3e<]mbI%.<EQ9g2*(.s#0Oi)l^' );
define( 'LOGGED_IN_KEY',    'A@b!IEF%cue8P-fOyp^.%;]|Z_ReT?CE^gHsG=lOM,YU@{`r7g&Flx#D,eg[T1}p' );
define( 'NONCE_KEY',        'uGhrT^wU%a_Pm-UM!J*wpPXQc^b/3-@lLBT@Ty4`y6K82 c_jmYon,kzJu`rvW~z' );
define( 'AUTH_SALT',        '`!LYXn$n<|]Ld#?OoGnZ*|xOZ77SkbB46w@`8V#nUD_LKXJRw}BTIrXOH3zPfPID' );
define( 'SECURE_AUTH_SALT', 'zjC@~?8JgU RVD,d]`,8xwl}=$]1%aJ_+F}XIEwQijN[]%=<>D-9>PN9_1a[VVqA' );
define( 'LOGGED_IN_SALT',   '@:%C}%[lHL!f@z~sKi!o^[@|F@RGy$8,JTG>JpX78d[Xc-.>UvW,Rr[R?A5.)0I{' );
define( 'NONCE_SALT',       'VE^k57Y>;3eUZyKZ@Ag)V);BB#{i3yKwX&$V7fFQL7V%+b|3oWJWqI!jB@nQ9<Nc' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tr_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
